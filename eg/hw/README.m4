include(docm4.m4)

 BARE-METAL HELLO WORLD
 ==============================

DOCM4_DIR_NOTICE

 Overview
 ------------------------------

 This directory contains a series of code examples that illustrate the
 step-by-step implementation of a x86 bare-metal "Hello World" program,
 which can be booted and executed on a real piece of hardware.

 The sequence starts as simple as a program written directly in machine
 code, which is then rewritten in pure assembly, and finally translated
 into C language.


DOCM4_INSTRUCTIONS

 Contents
 ------------------------------


 Take a look at the following examples, preferably in the suggested order.
 

 * eg-00.hex	    Bare-metal hardcore Hello World in machine code.
 
   		    This is a tough old-school version of the classical
		    Hello World program. It writes the string directly to the
		    BIOS video memory, character by character, one at a time.
		    A Hello World as you may have never made before, with
		    neither an OS, nor a programming language, not even
		    assembly; just bare machine code.

		    The source code is an ASCII file containing the opcodes of
		    the program in hexadecimal representation.

		    Build with it with a simple hex-to-binary converter:

		       make eg-00.bin


		    Test with the emulator

 		       make eg-00.bin/run

		    Transfer eg-00.bin to a USB sick 

		       make eg-00.bin/stick <your-device e.g. /dev/sdb>

		    and boot it in the real PC hardware. If it does not work,
		    please refer to "Note on booting the real hardware" ahead
		    in this document.


 * eg-01.asm        The same as eg-00.hex, but now in assembly code.

   		    This program implements literally the same algorithm as
		    eg-01.hex, but written in assembly code, using Intel
		    syntax. This is the assembly understood by NASM assembler,
		    used here to build the binary.

		    Compare eg-01.bin and eg-02-beta.1.bin, by disassembling
		    them with

		      make eg-00.bin/16i
		      make eg-01.bin/16i

                    or, alternatively, with the graphical tool

		      make diff eg-00.bin eg-01.bin BIT=16 ASM=intel

		    and see that the resulting object codes match.

		    We are forcing the disassembler to interpret the object
		    as code for a i8086 (16-bit) CPU, using intel asm syntax.


 * eg-02-alpha.asm  A variation of eg-01.asm, using a loop.

   		    This code does not work. Can you spot the problem?

 * eg-02-beta.asm   Same as eg-02-beta.asm, but fixed.

  		    Notice that now that we are dealing with offsets, we must
		    beware of the RAM load address. In this example, we handle
		    this by directly adding the offset into the code where
		    needed.
		    
 * eg-02.asm	    Same as eg-02-beta.2.asm, but using the 'org' directive.

   		    The directive 'org' causes all labels to be offset
		    to match the position where BIOS loads the program.

		    
		    Compare eg-02-beta.bin and eg-02.bin with

		      make eg-02-beta.bin/i16 
		      make eg-02.bin/i16

		    or

		      make i16 eg-02-beta.bin eg-02.bin
  		    
		    and see that the resulting object codes match.

		    Now it's opportune to observe that NASM is performing
		    both the assembling (object code generation) and
		    linking (build-time address relocation) steps.



* eg-03-beta.S      Like eg-02.asm, but translated into AT&T assembly syntax.

  		    One motivation to convert the assembly source code from
		    Intel to AT&T dialect is because we intend to rewrite
		    the running example in C language.  To that end, we will
		    use the powerful and flexible GNU compiler collection.
		    The thing is, GCC's toolchain speaks AT&T asm and does not
		    understand NASM's intel asm.

		    See note (2) for further detailing.

   		    The translation was made as literal as possible so as to
		    facilitate the comparison with eg-04.asm.

		    A noteworthy observation is that the build procedure of
		    this example involves two steps: assembling and linking.
		    The former translates assembly to object (machine) code,
		    and the latter remaps offsets to match the load address.
		    Those are the normal duties of the assembler and the linker.
		    With NASM, which we had been using previously, those
		    two functions are performed by the same program. In the
		    GNU development toolchain, differently, those duties are
		    performed by two distinct programs: the assembler (GAS,
		    whose executable program name is 'as') converts assembly
		    to object code (machine code), and then the linker (ld)
		    is used to remap the addresses offset to match the RAM
		    load address. GAS does not have an equivalent for the
		    .org directive that we used with NASM for this purpose;
		    we inform the linker directly via a command line option.

		    Moreover, whilst we had instructed NASM to produce a flat
		    binary, the version of GAS installed in our x86 / x86_64
		    platform outputs binaries in ELF format --- a structured
		    executable format that has much more than the executable
		    code section (e.g. header, relocation information and
		    other metadata). It is the liker who is responsible to
		    strip all extra sections and output a flat binary.
		    We tell ld to do that using another command-line option.

		    Finally, ld let us specify which will be the entry point
		    of the executable. This would be important for structured
		    file formats such as ELF (Linux) or PE (windows). For
		    us, it's the first byte. However, since ld dos expect it,
		    we pass this information in yet another command-line option.


 * eg-03.S	    Same as before, but with standard entry point name.

   		    This example is to illustrate that the default symbol name
		    for the entry point is _start. If we stick to it, we do
		    not need to pass it as an option for to the linker. 

		    Alternative:

		    Issuing the build rule with the command-line variable
		    ALT=1 selects an alternative recipe using a single GCC 
		    invocation (GCC then invokes the assembler and the linker
		    with appropriate arguments).

		    Disassemble (AT&T syntax, i8080 cpu) with 

		      make eg-02.bin/a16 
		      make eg-03.bin/a16

		      or

  		      make a16 eg-02.bin eg-03.bin 
		      
		    and see that they all binaries match, i.e. the output of
		    both assemblers (NASM and GAS) are the same.


 * eg-04-alpha.c   Like eg-03.S but rewritten in C and inline assembly (buggy)

   		    We use basic inline assembly. GCC should copy the asm code
                    as is to the output asm.  The two function-like macros
		    are expaned by the preprocessor in build time.

		    Caveats:

                    We declared the function with attribute 'naked' to prevent
                    GCC from generating extra asm code which is not relevant
                    here and may be omitted for readability. See comments
                    in the source file.

		    Notice that the string is placed before the executable code.
		    Since the code will be executed from the very first byte,
		    the program will not work. This is a problem.

		    Notice also that even if we move the string  to some place
		    after the executable code, the string is still allocated in
		    the read-only data section. This is also a problem because
		    the labels are offsets relative to the start of the section
		    where they are defined, and this the string label won't be
		    correctly accessible from within the .text section.

 * eg-04-beta.c	    Like eg-04-alpha.c, but the string is moved to the end.

   		    Observe the ad hoc tactic (aka plain hack) we used to work
		    around the compiler's default policy to allocate the
		    string in the .rodata section.


* eg-04-beta.1.c    Like eg-04-beta, but with asm code in a header file.

  		    Now the source looks a bit more like a C code.
  		    

 * eg-04.c	    Like eg-06-beta.c, but using a linker script.

   		    The provided eg-04.ld script handles several issues:

   		    - merge .rodata into .text section

		    as well as

   		    - add the the boot signature        
   		    - convert from elf to binary         
   		    - set load address                  
   		    - set entry point


		    The first feature frees our code from the hack on .rodata
		    problem, and the latter four simplify ld command-line options.


		    Compare eg-03.bin and eg-04.bin with

		    	make a16 eg-03.bin eg-04.bin

                    Both object codes are amost a perfect match, except by 

		    Bearing in mind that, since there is possibly more than
		    one way to implement the behavior specified by the C
		    source code into assembly form, it is not guaranteed
		    that both the handwritten eg-03.S and the GCC-produced
		    eg-03.S will match.
		    
		    Indeed if we inspect the disassembled objects, we may see
		    that GCC added a few extra instructions after the code.
		    Those are no-operation (NOP) and undefined (UD2)
		    instructions that are inserted by the compiler as a
		    security measure to assert that code is not executed
		    past the end of the legit code.  The appended
		    instructions however cause the string to be moved some
		    positions away from its original place in eg-03.S.
		    This is reflected in some other parts of the code.
		    

 * eg-05.c	    A rewrite of 04.c replacing macros with functions.

		    One thing to be noticed is that we had to change the
		    assembly to use register si where we had been using bx.

		    See note (3).

		    We alse had to set the location where the call instruction
		    should leave the return address for the callee.


 * eg-06.c	    Like eg-09 but using GCC extended assembly.

   		    Extended assembly allow us to

		      reference C variable within asm code;
		      generate unique labels across the compilation unit;
		      notify GCC about clobbered registers.

		    Build eg-09.bin and compare

   		       make diff eg-05_utils.s eg-06_utils.s

		       make diff eg-05_utils.bin eg-06_utils.bin

		    In extended asm GCC is allowed to optmize to modify the
		    output. We used volatile with extended asm to prevent
		    optimization. Even though, there are slight differences.

		    We handled the stack pointer in a better way.

 * eg-07.c	    Finally our masterpiece C-looking program.

   		    We did some changes

		       replaced asm rt0 with a C version
		       used the linker to set the stack location
		       used the linker to prepend rt0
		       changed functions name to look like C
		       jump to main from rt0
		       fixed writing routine not to output 0x0

		    We now return from main, rather than calling exit.

		    This time we can't use attribute naked in main();
		    otherwise the compiler removes the 'ret' instruction.
		    The assembly for eg-11.s is added some extra code that
		    is not fundamental here, as explained in eg-07.c comments.

   		    See comments in the file.


 Notes
 ------------------------------

 (1)   Original PC's BIOS used to read the MBR from the first 512 bytes of
       either the floppy or hard disks. Later on, when CD-ROM technology
       came about, it brought a different specification for the boot
       information, described in the iso9960 (the conventional CD file system
       format). Vendors then updated the BIOSes to detect whether the storage
       is either a floppy (or HD) or a CD-ROM, and apply the appropriate boot
       procedure. More recently, when USB flash devices were introduced, the
       approach adopted by BIOS vendors was not to specify a new boot procedure,
       but to emulate  some of the former devices. The problem is that this
       choice is not very consistent: some BIOSes would detect a USB stick as
       a floppy, whereas other BIOSes would see it as a CD (welcome to the
       system layer!).

       If your BIOS mimics the original PC, to make a bootable USB stick
       all you need to do is to copy the MBR code to its first 512 bytes:


   	  make stick IMG=foo.bin STICK=/dev/sdX


       On the other hand, if your BIOS insists in detecting your USB stick
       as a CD-ROM, you'll need to prepare a bootable iso9660 image as
       described in El-Torito specification [2]. 

       As for that, the GNU xorriso utility may come in handy: it prepares a
       bootable USP stick which should work in both scenarios. Xorriso copies
       the MBR code to the first 512 bytes to the USB stick and, in addition,
       it also transfer a prepared iso9660 to the right location. If you can't
       get your BIOS to boot and execute the example with the above (floppy)
       method, then try

         make stick IMG=foo.iso STICK=/dev/sdX


       We wont cover iso9660 El-Torito boot for CD-ROM, as newer x86-based
       computers now offers the Unified Extensible Firmware Interface meant
       to replace the BIOS interface of the original PC.  EFI is capable
       of decoding a format device, allowing the MBR code to be read from a
       standard file system. Although EFI is gradually turning original PC
       boot obsolete, however, most present day BIOSes offer a so called
       "legacy mode BIOS" and its useful to understand more sophisticated
       technologies, as low-level boot is still often found in legacy
       hardware and embedded systems.



 (2)  One reason we had better switch from nasm (Intel) to gas (AT&T)
      assembly here is because GCC (compiler) and NASM (assembler) do
      not go that well together. Actually, although GCC outputs AT&T syntax
      by default, it is true that GCC is capable of outputing Intel
      assembly if asked by means of the 'masm' option.  The problem is that
      the latter comes in specific Intel assembly dialect meant for GAS
      (GNU Assembler), which is not the same as that understood by NASM
      --- for instance, some directives (such as 'bit 16' vs .code16,
      times vs .fill etc.), data types ('dw' vs 'word'), and mnemonics
      (mov vs moveb, movel etc.) differ.

      A glimpse of those differences may be seen in egx-01.S.

      It's therefore not practical to ask GCC to generate intel assembly
      from C, and have the latter assembled by nasm; we would need to
      use GAS, instead anyway.

      As a side note, that is why we do not refer to assembly as a
      "programming language", in the same way we refer to the C language
      but, as the name implies, as an "assembling language". Diverse
      assembly standards vary in syntax and thereby constitute different 
      specifications. Moreover, the assembly code is dependent on the
      processor architecture e.g. i386 vs ARM etc.

      If we want to read the assembly generated by GCC, even if we ask
      for Intel syntax, we would need to learn a new dialect --- whose
      differences are not only a matter of syntax, but also of semantics.
      For instance, the way NASM compiler works with directive 'org'
      is very different from how GAS works in association to the linker
      (ld) to relocate code.  It so happens that it may be arguably more
      interesting learn the native GCC toolchain assembly dialect and
      proceed therefrom.


 (3)  Previously, we would do
		    
      		    AT&T syntax			Intel syntax

      		    msg(%bx)			[msg + bx]


      to iterate through the characters of the string located at msg, using bx
      as the index of the array.

      Now, rather than constant labeled position, our string may be anywhere
      and we pass it to the function as an address stored in %cx. The thing
      is, we can't do

		    AT&T syntax			Intel syntax

		    %cx(%bx)			[cx + bx]
		    
      Why not? Because the i8086 (16-bit precursos of x86) does not allows to.
      The way to traverse an array here is by using the base-index-scale

		  base address + index array * scale

      E.g.

		   (%bx, %si, 1)

      is the memory position starting from the base address %bx, advancing
      %si * 1 positions.  The scale may be e.g. 4 if we want to advance
      %si 32-bit integers.

      So, in our example, can we do

		  (%cx, %bx)

       No.

       In 16-bit real mode x86 allow us We limited choices

	     	   (%bx), (%bp), (%si), (%di),

	           (%bx,%si), (%bx,%di), (%bp,%si) and (%bp,%di)


        We than had to modify our code to use %si for iteration and %bx for
	the base. (Do the respective names "base register" and "source  index 
	register" for bx and si ring you a bell?)

	Do I really need to know all of this??? 

	Hump... know this you must, for intricate and deceptive the hardware
	may be... and fighting the darkness you shall, to bring consistency
	and balance where there would be only chaos and fear.

 
DOCM4_BINTOOLS_DOC


 References
 ------------------------------
 
 [1] Auxiliary program: syseg/src/hex2bin

 [2] El-Torito: https://wiki.osdev.org/El-Torito




