;; Global data calling convention
;; Easy to implement, not flexible (mostly in embedded systems)

	
global _start
	
section .bss		   ; Global variables for parameter passing
	arg1 resb 4
	arg2 resb 4

section .text

_start:
	mov dword [arg1], 5	   ; Copy values into global variables
	mov dword [arg2], 2
	call my_func		   ; Call function

	mov ebx, eax		   ; Exit with computed value
	mov eax, 1
	int 0x80

my_func:
	mov eax, dword [arg1]  ; Assume parameters are in global variables
	sub eax, dword [arg2]
	ret

