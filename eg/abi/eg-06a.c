/* Calling convention example: return in eax. */

/* int foo (void); */

int foo (void)
{
  return 42;
}


int main (void)
{
  return foo()+1;
}
