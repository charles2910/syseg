include(docm4.m4)dnl
DOCM4_HASH_HEAD_NOTICE([Makefile],[Makefile script.])

#
# Main examples and auxiliary examples
#

bin = eg-00.bin eg-01.bin eg-02.bin

binx =  egx-01.bin egx-02.bin

AUXDIR =../../tools#


all : $(bin) $(binx)

UPDATE_MAKEFILE

###########################################################                                    
##                                                                                             
## These are the rules of interest in this set of examples.                                    


## GAS assembly.
## We build with as and ld, using a linker script.

eg-00.o eg-01_alpha.o eg-01.o foo.o: %.o : %.S
	as --32 $< -o $@

eg-00.bin eg-01_alpha.bin eg-01.bin foo.bin: %.bin : %.o mbr-nort0.ld
	ld -melf_i386 --orphan-handling=discard  -T mbr-nort0.ld $(filter %.o, $^) -o $@

## C source code.
## We build the program using gcc, as and ld.

binc = eg-02 eg-03

$(binc:%=%.bin) : %.bin : %.o %_utils.o mbr.ld rt0.o
	ld -melf_i386 --orphan-handling=discard -T mbr.ld $*.o $*_utils.o -o $@

$(binc:%=%.o) $(binc:%=%_utils.o) rt0.o :%.o: %.s
	as --32 $< -o $@

$(binc:%=%.s) $(binc:%=%_utils.s) rt0.s :%.s: %.c
	gcc -m16 -O0 -I. -Wall -fno-pic -fcf-protection=none  --freestanding -S $< -o $@




#
# Test and inspect
#

# include $(AUXDIR)/bintools.makefile

.PHONY: clean clean-extra intel att 16 32 diss /diss /i16 /i32 /a16 /a32

#
# Extra auxiliary examples
#

egx-01.o egx-02.o : %.o : %.S
	as --32 $< -o $@

egx-01.bin egx-02.bin : %.bin : %.o mbr-nort0.ld
	ld -melf_i386 -T mbr-nort0.ld --orphan-handling=discard $(filter %.o, $^) -o $@

#
# Housekeeping
#

clean:
	rm -f *.bin *.elf *.o *.s *.iso *.img *.i
	make clean-extra

clean-extra:
	rm -f *~ \#*


DOCM4_MAKE_BINTOOLS


