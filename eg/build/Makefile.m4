include(docm4.m4)dnl
DOCM4_HASH_HEAD_NOTICE([Makefile],[Makefile script.])

AUXDIR =../../tools#

#
# Main examples and auxiliary examples
#

bin = eg-01 eg-02 eg-03-alpha eg-03 eg-04 eg-06 eg-06 
ppi = eg-05.i
arc = eg-07.a
lib = libeg-08.a

all: $(bin) $(ppi) $(arc) $(lib)

BARE= -fno-pic -fno-pie -fno-asynchronous-unwind-tables -fcf-protection=none

# Very simple example to illustrate the build steps

eg-01 : % : %.o %.i
	gcc -m32 -Wall -O0 $< -fno-pic -o $@

eg-01.o : %.o : %.s
	as -32 $< -o $@

eg-01.s : %.s : %.i
	gcc -Wall -O0 -m32 $(BARE) -Wl,--static -S $< -o $@

eg-01.i : %.i : %.c
	cpp -Wall -P $< -o $@

# Compilation unities

eg-02 eg-03 : % : %.c
	gcc -Wall $< -o $@

eg-03-alpha : % : %.c
	gcc -Wall $< -o $@
	@echo "Note: it's ok, we were expecting this warning"
	@echo

eg-04 : % : %.i
	gcc -Wall $< -o $@

eg-04.i : %.i : %.c %.h
	gcc -E -I. -P $< -o $@

eg-05.i : %.i : %.c
	gcc -E -I. -P $< -o $@

eg-05:
	@echo "Not a target; see README."

ifndef ALT
eg-06 : % : %.o %_foo.o %_bar.o
	gcc -Wall $^ -o $@
else
eg-06 : % : %.o lib%.a
	gcc -Wall $< -fno-pie -o  $@ -L. -l$*
endif

eg-06.o eg-06_foo.o eg-06_bar.o eg-06_baz.o: %.o : %.s 
	gcc -Wall -c -fno-pie $< -o $@

eg-06.s eg-06_foo.s eg-06_bar.s eg-06_baz.s : %.s : %.i
	gcc -S $< -fno-pic -o $@

eg-06.i : %.i : %.c %.h
	gcc -E -I. -P $< -o $@

eg-06_foo.i  eg-06_bar.i eg-06_baz.i : %.i : %.c 
	gcc -E -I. -P $< -o $@

ifeq ($(ALT), 1)
libeg-06.a : eg-06_foo.o eg-06_bar.o
	ar rs $@ $^
endif

ifeq ($(ALT),2)
libeg-06.a : eg-06_foo.o eg-06_bar.o eg-06_baz.o
	ar rs $@ $^
endif

eg-07.a :
	echo AAAAAAAAAA > f1
	echo BBBBBBBBBB > f2
	ar r $@ f1 f2




.PHONY: clean

clean:
	rm -f $(bin) *.o *.i *.a main *.s *~ f1 f2 f

DOCM4_MAKE_BINTOOLS
