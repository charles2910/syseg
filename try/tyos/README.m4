include(docm4.m4)

 Tiny OS - A real-mode disk operating system (tyOS)
 ==============================================
DOCM4_DIR_NOTICE

 Overview
 ------------------------------

 In this programming exercise the student is requested to implement a very
 elementary DOS-like operating system. The program should boot from a USB stick
 formatted with a FAT file system, and should be loaded using the BIOS legacy
 boot mechanism. Like DOS or CP/M, the OS should present the user a terminal
 prompt, at which the user may type commands. A command may be either a
 built-in function, in which case the function is performed, or the name of
 a regular file stored in the disk (the USB stick). If there exists such
 file, it should be interpreted as an executable program: the OS should load
 the program into the RAM and execute it thoroughly. Upon termination, the
 user program should return execution to the OS. Both the OS and user program
 should run in x86 real mode.

 Lore: it's not everyday that one programs an operating system. Even a small
       one. If you take the challenge, you'll be following the tracks of
       brave masters that one day created the first PC operating systems
       back in the 80's, which then evolved into today complex systems.
       You shall give your project a name! It must not be tyos; that is
       already taken. Choose wisely, and proudly ... it may be the start
       of the next thing after Linux or FreeBSD.
       

 * Note: Please, read this document thoroughly before starting to code.
       

 Detailed instructions and tips.
 ------------------------------

 While you may prefer to perform the programming exercise going through a
 distinct sequence of steps, this is one suggested roadmap.
 
 1) Boot disk creator

   Implement the program mkboot which creates the bootable USB disk with
   your OS. As a kickoff tip, perhaps you might find useful to take a look
   at the source code examples of the series in syseg/eg/format. You may,
   for instance, create a modified version of the example programs to
   install a custom bootstrap program in the disk.

   The bootstrap image will have to be small enough to fit the available are
   within the 512-byte boot sector.  Since your OS will likely (certainly)
   be larger than that, you will probably need a two-stage boot scheme:
   use the bootstrap code as a bootloader, and use it to load the OS kernel.
   One way to do that is to store your OS in extra reserved sectors of
   the boot disk's FAT file system, and program your bootloader to load the
   extra reserved sectors into the RAM. You may take a look, for instance at
   the source code examples in syseg/try/bl.


 2) Your awesome OS

   Your OS should exhibit a prompt and read the keyboard. When the user types
   the name of a built-in command and press the Enter key, the OS should
   execute the command and return the prompt. If the word typed by the user is
   not a built-in command, the OS should search for a file with that name in
   the boot disk file system. If such a program is found, the OS should load
   it and execute it.

   That implies that your OS should be able to interpret a FAT file system.
   If you use FAT12, for instance, you may find useful to have a look at the
   examples in syseg/eg/format.

   We're not covering multitasking: the OS will load one single user
   program at a time and let it execute until termination. 

 3) User programs

    In order to illustrate the OS in operation, you'll need to write some
    user programs.  Those programs will execute in 16-bit real-mode. While it
    is possible write them directly in assembly, you are strongly encouraged
    to program in C (with some inline assemble where needed) --- in this case,
    you'll probably find useful the idea of creating a small library that your
    user programs can reuse.

    Either way, you should instrument the programs to return control to the
    kernel upon completion.

 A Makefile is provided with several handy commands to inspect the contents of
 binary images, disassemble and compara object files. See instructions at the
 Appendix section bellow. For clarity, this Makefile includes a second, blank
 Make script build.mk that you can costumize as needed.  

 Badges of honor
 ------------------------------

 These are some challenges that would yield you legitimate bragging rights.

 * You may well use FAT12 file system in your bootdisk. If that is not enough
   for your inner hacker, though, you may opt for FAT16 or FAT32, instead.
   That would be worth many XPs. You might start by checking [[1]].

 * During execution, user programs can call BIOS services. If you want your
   kernel to operate even more like a full-featured DOS, you will like the
   idea of implementing some syscall-like functionaly. For instance, in MS-DOS,
   programs can use the software interruption INT 21h to request DOS services.
   A call to DOS, in this scenario, works like the calls to the BIOS: both
   are triggered by the same instruction INT. When the instruction is executed,
   the processor looks at a particular region of memory called Interrupt
   Vector Table, at a specific location in the RAM.  Each entry in this table
   is the address to which the execution should jump when a given interrupt
   is issued. For instance, for INT 10h, the corresponding 4-byte entry
   (at the position 10h*4 = 40 from the beginning of the table) is the address
   to which the execution jump when the program issues the BIOS video service.
   Some entries in the IVT are set by BIOS upon booting, some may be altered
   by the OS. DOS, for instance, maps the entry of INT 21h for its services.
   If you are so brave as to dare implementing kernel services via INT
   instruction like DOS, you may like to check these references [[2,3,4]].

 * An alternative way of preparing the boot disk is storing the kernel as a
   regular file in the device's file system, and using the disk creator
   to locate the cluster address of the kernel and write this information
   in the bootloader area.  The bootloader then can check the device's
   FAT to load the remaining clusters that compose the kernel (Would that
   fit the boot sector? Or additional extra reserved segments are needed
   in the file system?). Cool ah? 


   [[1]] Wikipedia, Design of the FAT file system
       https://en.wikipedia.org/wiki/Design_of_the_FAT_file_system

   [[2]] Wikipedia, https://en.wikipedia.org/wiki/Interrupt_descriptor_table
       https://en.wikipedia.org/wiki/Interrupt_descriptor_table

   [[3]] OSDev.org, Interrupt Vector Table
       https://wiki.osdev.org/Interrupt_Vector_Table

   [[4]] Stainslavs, DOS IVT
       https://stanislavs.org/helppc/int_table.html

DOCM4_EXPORT_DIRECTIONS

DOCM4_CLOSING_WORDS


 APPENDIX A
 ------------------------------
 
DOCM4_MAKE_BINTOOLS_DOC

 
